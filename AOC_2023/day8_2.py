data = open('day8.txt').read().splitlines()

#data = open('day8_test.txt').read().splitlines()


def ind(a):
    return 1 if a == 'R' else 0

RL = data.pop(0)
t = len(RL)

data.pop(0)

dico = dict()

for ligne in data:
    l = ligne.split()
    dico[l[0]] = (l[2][1:-1], l[3][:-1])

depart = []
for c in dico:
    if c[2] == 'A':
        depart += [c]
n = len(depart)


step = 0
suite = 0

print(depart)
while suite < 1:
    suite = 0
    d = ind(RL[step%t])
    for i in range(1):
        depart[i] = dico[depart[i]][d]
        suite += (depart[i][2] == 'Z')
    step += 1
    print(depart)




print(step)
print(depart)
#PPCM de:
#18113 0
#13201 3
#20569 1
#21797 2
#24253 4
#22411 5
