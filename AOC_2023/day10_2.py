import sys
sys.setrecursionlimit(10000)
#data = open('day10_test_7.txt').read().splitlines()

data = open('day10.txt').read().splitlines()

E = "E"
W = "W"
N = "N"
S = "S"
V = "|"
H = "-"
L = "L"
J = "J"
T = "7"
F = "F"

G = "."

dicoLow = {V: "v", H: "h", L: "l", J:"j", T: "t", F: "f"}
dicoN = {V: S, L: E, J: W}
dicoS = {V: N, T: W, F: E}
dicoE = {H: W, L: N, F: S}
dicoW = {H: E, J: N, T: S}
sortie = {N:dicoN, S:dicoS, E:dicoE, W: dicoW}
class Pipe:

    def __init__(self, forme, begin, li, co):
        self.forme = forme # caractère
        self.begin = begin # E, W, N, S
        self.li = li # ligne, de gauche à droite
        self.co = co # colonne, de haut en bas

    def out(self):
        return sortie[self.begin][self.forme]

    def voisin(self):
        suite = self.out()
        if suite == E:
            cos = self.co + 1
            lis =self.li
            debut = W
        elif suite == W:
            cos = self.co - 1
            lis = self.li
            debut = E
        elif suite == N:
            cos = self.co
            lis = self.li - 1
            debut = S
        elif suite == S:
            cos = self.co
            lis = self.li + 1
            debut = N
        car = data[lis][cos]
        return Pipe(car, debut, lis ,cos)

    def __str__(self):
        return 'case: ' + self.forme + ' entrée ' + self.begin + ' sortie ' + self.out()


i = 0
for i in range(len(data)):
    l = data[i]
    for j in range(len(l)):
        if l[j] == 'S':
            lid = i
            print(l)
            cod = j
# S correspond à F en regardant dans les données pour day10.txt
# S correspond à F pour day10_2.txt
# S correspond à F pour days10_1.txt
# S correspond à F pour day10_4.txt
# S correspond à F pour day10_3.txt
# S corresopnd à 7 pour day10_7.txt

for ligne in data:
    print(ligne)
s = 0

depA = Pipe(F, S, lid, cod)
depB = Pipe(F, E, lid, cod)

#pour day10_7.txt
#depA = Pipe(T, S, lid, cod) # pour day10_7.txt
#depB = Pipe(T, W, lid, cod)

data[depA.li] = data[depA.li][:depA.co] +  dicoLow[depA.forme] +data[depA.li][depA.co+1:]
        

def chemin(A, B):
    # A et B de type Pipe, t entier
    # fonction récursive
    # renvoie entier
    if A.co != B.co or A.li != B.li:
        data[A.li] = data[A.li][:A.co] +  dicoLow[A.forme] +data[A.li][A.co+1:]
        data[B.li] = data[B.li][:B.co] +  dicoLow[B.forme] +data[B.li][B.co+1:]
        # la chance a voulu qu'il n'y ait pas de chemin sur les bords
        A = A.voisin()
        B = B.voisin()
        return chemin(A, B)
    else:
         data[A.li] = data[A.li][:A.co] +  dicoLow[A.forme] +data[A.li][A.co+1:]
    
chemin(depA.voisin(), depB.voisin())

for ligne in data:
    print(ligne)
    
def count(chaine):
    bord = 0
    result = 0
    for car in chaine:
        if car in {"v", "t", "f"}:
            bord +=1
        else:
            if bord % 2 == 1 and car not in {"v", "t", "f", "j", "l", "h"}:
                print(chaine)
                result +=1
    return result

c = 0
for ligne in data:
    c += count(ligne)
    print(c)


    

