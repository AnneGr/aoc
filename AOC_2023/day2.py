data = open('day2.txt').read().splitlines()

data_test = open('day2_test.txt').read().splitlines()

bag = (12, 13, 14)# red, green, blue


def unpack(line):
    ID = 0
    if line != []:
        i=5
        while line[i]!=':':
            i += 1
        ID = line[4:i]
    return i, ID


def doable(line):
    d, id = unpack(line)
    games = line[d+1:].split(';')
    for play in games:
        colors = play.split(',')
        for color in colors:
            if color.split()[1] == 'red':
                val =  int(color.split()[0])
                if val >bag[0]:
                    return 0
            if color.split()[1] == 'green':
                val =  int(color.split()[0])
                if val>bag[1]:
                    return 0
            if color.split()[1] == 'blue':
                val =  int(color.split()[0])
                if val>bag[2]:
                    return 0
    return int(id)



somme = 0
for ligne in data:
    somme += doable(ligne)


print(somme)
    
    


