data = open('day8.txt').read().splitlines()

#data = open('day8_test.txt').read().splitlines()


def ind(a):
    return 1 if a == 'R' else 0
    
RL = data.pop(0)
t = len(RL)

data.pop(0)

dico = dict()



for ligne in data:
    l = ligne.split()
    dico[l[0]] = (l[2][1:-1], l[3][:-1])

dir = 'AAA'
step = 0

while dir != 'ZZZ':
    d = ind(RL[step%t])
    dir = dico[dir][d]
    step += 1
    print(dir)


print(step)
