#data = open('day9_test.txt').read().splitlines()

data = open('day9.txt').read().splitlines()

def diff(seq):
    n = len(seq)
    res = []
    for i in range(1, n):
        res += [seq[i]-seq[i-1]]
    return res
def isZero(seq):
    res = False
    for i in range(len(seq)):
        if seq[i] == 0:
            res = True
        else:
            return False
    return res
def pred(seq):
    res = 0
    for i in range(len(seq)):
        res += seq[i][-1]
    return res

predict = 0
for ligne in data:
    sequence = [int(i) for i in ligne.split()]
    suite = [sequence]
    sequence = diff(sequence)
    suite += [sequence]
    while not isZero(sequence):
        sequence = diff(sequence)
        suite += [sequence]
    predict += pred(suite)

print(predict)
