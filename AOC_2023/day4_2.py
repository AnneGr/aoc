data = open('day4.txt').read().splitlines()

#data = open('day4_test.txt').read().splitlines()

gain = [1 for i in range(205)]



i=0
for ligne in data:
    ligne=ligne.split()
    gain_temp = gain[i: i+11]
    gagnants = {int(val) for val in ligne[2:12]}
    #gagnants = {int(val) for val in ligne[2:7]}
    #2 à 7 pour test, 12 pour input
    #carte = {int(val) for val in ligne[8:]}
    carte = {int(val) for val in ligne[13:]}
    #à partir de 8 pour test, 13 sinon
    points=0
    for e in carte:
        if e in gagnants:
            points+=1
    for k in range(i+1, i+points+1):
        gain[k]+=gain_temp[0]
    i += 1

somme = 0
for k in range(205):
    somme += gain[k]
print(somme)
