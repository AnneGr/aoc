class Map:
    def __init__(self, source, dest):
        self.source = source
        self.dest = dest
        self.map = [] #liste de tuples

    def table(self, val_dest, val_source, longeur):
        self.map += [(val_dest, val_source, longeur)]

    def conversion(self, val_source):
        for tbl in self.map:
            if int(tbl[1]) <= val_source and val_source < int(tbl[1]) + int(tbl[2]):
                return int(tbl[0]) + (val_source - int(tbl[1]))
        return val_source

    def __str__(self):
        return 'source:' +  str(self.source) + '\ndestination: ' + str(self.dest) + '\ntables de conversion: ' + str(self.map)



data = open('day5.txt').read().splitlines()

#data = open('day5_test.txt').read().splitlines()

premiere_ligne = data[0].split()
graines = [int(premiere_ligne[i]) for i in range(1, len(premiere_ligne))]


seed_to_soil = Map('seed', 'soil')
soil_to_fertilizer = Map('soil', 'fertilizer')
fertilizer_to_water = Map('fertilizer', 'water')
water_to_light = Map('water', 'light')
light_to_temperature = Map('light', 'temperature')
temperature_to_humidity = Map('temperature', 'humidity')
humidity_to_location = Map('humidity', 'location')


for ligne in data[2:]:
    if len(ligne)>0 and not ligne[0].isdigit():
        [source, to, dest] = ligne.split('-')
    match source:
       case 'seed':
           obj = seed_to_soil
       case 'soil':
           obj = soil_to_fertilizer
       case 'fertilizer':
           obj = fertilizer_to_water
       case 'water':
           obj = water_to_light
       case 'light':
           obj = light_to_temperature
       case 'temperature':
           obj = temperature_to_humidity
       case 'humidity':
           obj = humidity_to_location
    if len(ligne)>0 and ligne[0].isdigit():
        [vd, vs, t] = ligne.split()
        obj.table(vd, vs, t)



locations = []
for graine in graines:
    loc =humidity_to_location.conversion(temperature_to_humidity.conversion(light_to_temperature.conversion(water_to_light.conversion(fertilizer_to_water.conversion(soil_to_fertilizer.conversion(seed_to_soil.conversion(graine)))))))
    locations += [loc]

print(min(locations))
