data = open('day2.txt').read().splitlines()

data_test = open('day2_test.txt').read().splitlines()

#bag = (12, 13, 14)# red, green, blue


def unpack(line):
    ID = 0
    if line != []:
        i=5
        while line[i]!=':':
            i += 1
        ID = line[4:i]
    return i, ID


def power(line):
    d, id = unpack(line)
    games = line[d+1:].split(';')
    max_color = [0, 0, 0]
    for play in games:
        colors = play.split(',')
        for color in colors:
            if color.split()[1] == 'red':
                val =  int(color.split()[0])
                max_color[0]=max(max_color[0], val)

            if color.split()[1] == 'green':
                val =  int(color.split()[0])
                max_color[1]=max(max_color[1], val)
                     
            if color.split()[1] == 'blue':
                val =  int(color.split()[0])
                max_color[2]=max(max_color[2], val)
    return max_color[0]*max_color[1]*max_color[2]
                



somme = 0
for ligne in data:
    somme += power(ligne)
    

print(somme)
    
    


