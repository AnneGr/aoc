import sys
sys.setrecursionlimit(10000)
#data = open('day11_test_1.txt').read().splitlines()

data = open('day11.txt').read().splitlines()

def affiche(tableau, nom):
    print(nom)
    for ligne in tableau:
        print(ligne)

def transpo(tableau):
    t1 = len(tableau[0]) # largeur
    t2 = len(tableau) # hauteur
    res =  [[0 for _ in range(t2)] for _ in range(t1)]
    for i in range(t2):
        for j in range(t1):
            res[j][i] = tableau[i][j]
    return res

def vide(suite):
    for c in suite:
        if c == "#":
            return False
    return True

def galaxies(tableau):
    res = set()
    for li in range(len(tableau)):
        for co in range(len(tableau[0])):
            if tableau[li][co] == "#":
                res.add((li, co))
    return res

def distance(tab ,a, b):
    (xa, ya) = a # noms de variables mal choisis
    (xb, yb) = b
    somme = 0
    h = yb-ya
    v = xb-xa
    if h >= 0:
        for i in range(h):
            somme += tab[xa][ya + i + 1]
    else:
        for i in range(-h):
            somme += tab[xa][ya - i - 1]
    if v >= 0:
        for i in range(v):
            somme += tab[xa + i + 1][yb]
    else:
        for i in range(-v):
            somme += tab[xa - i - 1][yb]
    return somme

N = 1000000
expended = []

# largeur = len(data[0])
# hauteur = len(data)

#affiche(data, "data")


# expansion des lignes
for ligne in data:
    if vide(ligne):
        expended += [[N for _ in ligne]]
    else:
        expended += [[1 if x != "#" else "#" for x in ligne]]


#affiche(expended, "expended")

# transposition puis expansion des lignes de la matrice transposée
temp = transpo(expended)
temp_expended_col = []

for ligne in temp:
    if vide(ligne):
        temp_expended_col +=  [[N*x for x in ligne]]
    else:
        temp_expended_col += [[x for x in ligne]]
        
# transposition de l'expansion complète
transpo_expended = transpo(temp_expended_col)

#affiche(transpo_expended,"")

# récupération des positions des galaxies
liste_galaxies = galaxies(transpo_expended)

# 1 aux emplacements des galaxies
for ligne in transpo_expended:
    for index, value in enumerate(ligne):
        if value == "#":
            ligne[index] = 1
#affiche(transpo_expended, "transpo expended avec des 1")
#print(liste_galaxies)

total = 0
while liste_galaxies != set():
    x = liste_galaxies.pop()
    for y in liste_galaxies:
        total += distance(transpo_expended, x, y)

print(total)


