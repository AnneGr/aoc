class Hand:

    def __init__(self):
        self.hand = ""
        self.bid = 0
        self.dico = dict()

    def setHand(self, hand):
        self.hand = hand

    def setBid(self, bid):
        self.bid = int(bid)

    def dictHand(self):
        for c in self.hand:
            if c in self.dico:
                self.dico[c] += 1
            else:
                self.dico[c] = 1
        if 'J' in self.dico:
            if self.dico['J'] == 5:
                return "five"
            else:
                n = self.dico['J']
                del self.dico['J']
                cle_max = max(self.dico, key=self.dico.get)
                self.dico[cle_max] += n
        
        if len(self.dico) == 1:
            return "five"
        elif len(self.dico) == 2:
            if 4 in self.dico.values():
                return "four"
            elif 3 in self.dico.values():
                return "full"
        elif len(self.dico) == 3:
            if 3 in self.dico.values():
                return "three"
            elif 2 in self.dico.values():
                return "two"
        elif len(self.dico) == 4:
            return "one"
        elif len(self.dico) == 5:
            return "high"

    def __lt__(self, handB):
        ha = self.hand
        hb = handB.hand
        l = len(ha)
        for i in range(l):
            if val[ha[i]] < val[hb[i]]:
                return True
            elif val[ha[i]] > val[hb[i]]:
                return False
        return True


    def __repr__(self):
        return self.hand + " " + str(self.bid)

val = {'A':14, 'K':13, 'Q':12, 'J':1, 'T':10}
for c in range(2,10):
    val[str(c)] = c



#data = open('day7_test.txt').read().splitlines()

data = open('day7.txt').read().splitlines()

five = []
four = []
full = []
three = []
two = []
one = []
high = []


for ligne in data:
    l = ligne.split()
    if len(l)>0:
        m = Hand()
        m.setHand(l[0])
        m.setBid(l[1])
        match m.dictHand():
            case "five":
                five += [m]
            case "four":
                four += [m]
            case "full":
                full += [m]
            case "three":
                three += [m]
            case "two":
                two += [m]
            case "one":
                one += [m]
            case "high":
                high += [m]

res = sorted(high)+sorted(one)+sorted(two)+sorted(three)+sorted(full)+sorted(four)+sorted(five)

c = 0
for i in range(len(res)):
    c += (res[i].bid)*(i+1)



print(c)
