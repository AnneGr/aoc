import sys
sys.setrecursionlimit(10000)
#data = open('day10_test_4.txt').read().splitlines()

data = open('day10.txt').read().splitlines()

E = "E"
W = "W"
N = "N"
S = "S"
V = "|"
H = "-"
L = "L"
J = "J"
T = "7"
F = "F"
dicoN = {V: S, L: E, J: W}
dicoS = {V: N, T: W, F: E}
dicoE = {H: W, L: N, F: S}
dicoW = {H: E, J: N, T: S}
sortie = {N:dicoN, S:dicoS, E:dicoE, W: dicoW}
class Pipe:

    def __init__(self, forme, begin, li, co):
        self.forme = forme # caractère
        self.begin = begin # E, W, N, S
        self.li = li # ligne, de gauche à droite
        self.co = co # colonne, de haut en bas

    def out(self):
        return sortie[self.begin][self.forme]

    def voisin(self):
        suite = self.out()
        if suite == E:
            cos = self.co + 1
            lis =self.li
            debut = W
        elif suite == W:
            cos = self.co - 1
            lis = self.li
            debut = E
        elif suite == N:
            cos = self.co
            lis = self.li - 1
            debut = S
        elif suite == S:
            cos = self.co
            lis = self.li + 1
            debut = N
        car = data[lis][cos]
        return Pipe(car, debut, lis ,cos)

    def __str__(self):
        return 'case: ' + self.forme + ' entrée ' + self.begin + ' sortie ' + self.out()


i = 0
for i in range(len(data)):
    l = data[i]
    for j in range(len(l)):
        if l[j] == 'S':
            lid = i
            print(l)
            cod = j
# S correspond à F en regardant dans les données pour day10.txt
# S correspond à F pour day10_2.txt
# S correspond à F pour days10_1.txt
# S correspond à F pour day10_4.txt
# S correspond à F pour day10_3.txt

s = 0

depA = Pipe(F, S, lid, cod)
depB = Pipe(F, E, lid, cod)

print(depB)
print(depB.voisin())
def chemin(A, B, t):
    # A et B de type Pipe, t entier
    # fonction récursive
    # renvoie entier
    if A.co != B.co or A.li != B.li:
        t += 1
        A = A.voisin()
        B = B.voisin()
        print(t)
        return chemin(A, B, t)
    else:
        return t+1
    
print(chemin(depA.voisin(), depB.voisin(), 0))

