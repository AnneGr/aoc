#data = open('day6_test.txt').read().splitlines()

data = open('day6.txt').read().splitlines()

time_str = data[0].split()[1:]
time = [int(t) for t in time_str]

distance_str = data[1].split()[1:]
distance = [int(d) for d in distance_str]

def dist(vitesse, duree):
    return (duree-vitesse)*vitesse

c = [0]*len(time)
for i in range(len(time)):
    d = time[i]
    for v in range(d+1):
        if dist(v, time[i]) > distance[i]:
            c[i] += 1

tot = 1
for i in range(len(time)):
    tot = tot * c[i]  

print(tot)

