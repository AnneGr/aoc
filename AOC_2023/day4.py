data = open('day4.txt').read().splitlines()

#data = open('day4_test.txt').read().splitlines()

somme = 0
for ligne in data:
    ligne=ligne.split()
    gagnants = {int(val) for val in ligne[2:12]}#2 à 7 pour test
    carte = {int(val) for val in ligne[13:]}#à partir de 8 pour test
    points=0
    for e in carte:
        if e in gagnants:
            points+=1
    if points!=0:
        points = 2**(points-1)
    somme += points
print(somme)
        
    
