data = open('day1.txt').read().splitlines()

data_test = open('day1_test.txt').read().splitlines()

somme = 0

def first(line):
    for c in line:
        if c.isdigit():
            return int(c)

for ligne in data:
    val = first(ligne)*10+first(ligne[::-1])
    somme += val
    
print(somme)


