data = open('day1.txt').read().splitlines()

data_test = open('day1_2_test.txt').read().splitlines()

digits_dico = {'one':1,'two':2, 'three':3, 'four':4, 'five':5, 'six':6, 'seven':7, 'eight':8, 'nine':9}
digits = {'one','two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'}
begin = {digit[0] for digit in digits}
end = {digit[len(digit)-1] for digit in digits}
print(begin)
print(end)

def suite(car, ligne):
    if car == 'o' and len(ligne)>=3:
        if 'one'==ligne[0:3]:
            return ('one',1)
    if car == 't' and len(ligne)>=3:
        if 'two'== ligne[0:3]:
            return ('two', 2)
    if car == 't' and len(ligne)>=5:
        if 'three'== ligne[0:5]:
            return ('three', 3)       
    if car == 'f' and len(ligne)>=4:
        if 'four'== ligne[0:4]:
            return ('four', 4)  
    if car == 'f' and len(ligne)>=4:
        if 'five'== ligne[0:4]:
            return ('five', 5) 
    if car == 's' and len(ligne)>=3:
        if 'six'== ligne[0:3]:
            return ('six', 6)
    if car == 's' and len(ligne)>=5:
        if 'seven'== ligne[0:5]:
            return ('seven', 7)
    if car == 'e' and len(ligne)>=5:
        if 'eight'== ligne[0:5]:
            return ('eight', 8)
    if car == 'n' and len(ligne)>=4:
        if 'nine'== ligne[0:4]:
            return ('nine', 9)
    return (car, 0)


def pred(car, ligne):
    if car == 'r' and len(ligne)>=4:
        if 'four'==ligne[-4:]:
            return ('four', 4)
    if car == 'e' and len(ligne)>=3:
        if 'one'==ligne[-3:]:
            return ('one',1)
    if car == 'e' and len(ligne)>=4:
        if 'five'==ligne[-4:]:
            return ('five',5)
        if 'nine'==ligne[-4:]:
            return ('nine',9)
    if car == 'e' and len(ligne)>=5:
        if 'three'==ligne[-5:]:
            return ('three',3)
    if car == 'n' and len(ligne)>=5:
        if 'seven'== ligne[-5:]:
            return ('seven', 7)
    if car == 'x' and len(ligne)>=3:
        if 'six'== ligne[-3:]:
            return ('six', 6)       
    if car == 'o' and len(ligne)>=3:
        if 'two'== ligne[-3:]:
            return ('two', 2) 
    if car == 't' and len(ligne)>=5:
        if 'eight'== ligne[-5:]:
            return ('eight', 8)
    return (car, 0)


print(digits)

def first(line):
    while line != []:
        c = line[0]
        if c.isdigit():
            return int(c)
        elif c in begin:
            if suite(c, line)[0] in digits:
                return suite(c, line)[1] 
            else:
                line = line[1:]                     
        else:
            line = line[1:]
    return 0

def last(line):
    while line != []:
        c = line[len(line)-1]
        if c.isdigit():
            return int(c)
        elif c in end:
            if pred(c, line)[0] in digits:
                return pred(c, line)[1]  
            else:
                line = line[:-1]                 
        else:
            line = line[:-1]
    return 0
 
 

somme = 0  
for ligne in data:
    val = first(ligne)*10+last(ligne)
    somme += val
    
    
print(somme)
# 57193 trop haut

