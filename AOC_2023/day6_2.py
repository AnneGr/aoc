#data = open('day6_test.txt').read().splitlines()

data = open('day6.txt').read().splitlines()

time_str = data[0].split()[1:]
time = [int(t) for t in time_str]

distance_str = data[1].split()[1:]
distance = [int(d) for d in distance_str]

def dist(vitesse, duree):
    return (duree-vitesse)*vitesse

c = 0

for v in range(53717881):
    if dist(v, 53717880) > 275118112151524:
        c += 1
 

print(c)

