data = open('day3.txt').read().splitlines()

#data = open('day3_test.txt').read().splitlines()

pas_sym = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'}

largeur = len(data[0])
hauteur = len(data)

def select_num(line, i):
    str_num = ''
    while len(line)>0:
        c = line[0]
        line=line[1:]
        if c.isdigit():
            str_num += c
            while len(line)>0 and line[0].isdigit():
                c=line[0]
                line=line[1:]
                str_num += c
            return str_num, i
        i += 1
    return 'ligne vide', -1


def search_sym(stage, str_val, col):
    taille = len(str_val)
    g = max(0, col -1)
    d = min(col + taille + 1, largeur)
    if stage == 0:
        for k in range(g, d):
            if data[stage+1][k] not in pas_sym:
                return  int(str_val)
    elif stage == hauteur-1:
        for k in range(g, d):
            if data[stage-1][k] not in pas_sym:
                return int(str_val)
    else:
        for k in range(g, d):
            if data[stage-1][k] not in pas_sym:
                return int(str_val)
        for k in range(g, d):
            if data[stage+1][k] not in pas_sym:
                return int(str_val)
    if data[stage][g] not in pas_sym:
        return int(str_val)
    if data[stage][d-1] not in pas_sym:
        return int(str_val)
    return 0

somme = 0
etage = 0

for ligne in data:
    i=0
    ligne_temp = ligne
    num, i = select_num(ligne_temp, i)
    while i >=0:
        somme += search_sym(etage, num, i)
        i += len(num)
        ligne_temp=ligne[i:]
        num, i = select_num(ligne_temp, i)
    etage += 1

print(somme)

