import sys
sys.setrecursionlimit(10000)
#data = open('day11_test_1.txt').read().splitlines()

data = open('day11.txt').read().splitlines()

def affiche(tableau, nom):
    print(nom)
    for ligne in tableau:
        print(ligne)
def transpo(tableau):
    t1 = len(tableau[0]) # largeur
    t2 = len(tableau) # hauteur
    res =  [[0 for _ in range(t2)] for _ in range(t1)]
    for i in range(t2):
        for j in range(t1):
            res[j][i] = tableau[i][j]
    return res

def vide(suite):
    for c in suite:
        if c == "#":
            return False
    return True

def galaxies(tableau):
    num = 0
    res = set()
    for li in range(len(tableau)):
        for co in range(len(tableau[0])):
            if tableau[li][co] == "#":
                num += 1
                tableau[li][co] = num
                res.add((li, co))
    return res

def distance(a, b):
    (xa, ya) = a
    (xb, yb) = b
    return abs(xb-xa)+abs(yb-ya)
expended = []

# largeur = len(data[0])
# hauteur = len(data)

# affiche(data, "data")

for ligne in data:
    if vide(ligne):
        expended += [ligne] + [ligne]
    else:
        expended += [ligne]

# Largeur = len(expended[0])
# Hauteur = len(expended)

temp = transpo(expended)
temp_expended_col = []

for ligne in temp:
    if vide(ligne):
        temp_expended_col += [ligne] + [ligne]
    else:
        temp_expended_col += [ligne]
transpo_expended = transpo(temp_expended_col)

liste_galaxies = galaxies(transpo_expended)

print(liste_galaxies)

total = 0
while liste_galaxies != set():
    x = liste_galaxies.pop()
    for y in liste_galaxies:
        total += distance(x, y)

print(total)


