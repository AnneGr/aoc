data = open('day3.txt').read().splitlines()

#data = open('day3_test.txt').read().splitlines()

largeur = len(data[0])
hauteur = len(data)

def select_star(line, i):
    while len(line)>0:
        c = line[0]
        line=line[1:]
        if c == '*':
            return i
        i += 1
    return -1

def num(ligne, colonne):
    k = colonne
    c= ''
    while k>=0 and data[ligne][k].isdigit():
        c = data[ligne][k] + c
        k = k -1
    k = colonne
    while k+1<largeur and data[ligne][k+1].isdigit():
        k += 1
        c = c + data[ligne][k]
    return c

def nb_adj(trio, ligne, colonne):
    if len(trio)==3 and (trio[0].isdigit() and (not trio[1].isdigit()) and trio[2].isdigit()):
        return [num(ligne, colonne), num(ligne, colonne+2)]
    elif trio[0].isdigit():
        return [num(ligne, colonne)]
    elif len(trio)>1 and trio[1].isdigit():
        return [num(ligne, colonne+1)]
    elif len(trio)>2 and trio[2].isdigit():
        return [num(ligne, colonne+2)]
    else:
        return []

def search_gear(stage, col):
    adjacent = 0
    val=[]
    g = max(0, col -1)
    d = min(col + 2, largeur)
    bord = ''
    if stage == 0:
        for k in range(g, d):
            bord += data[stage+1][k]
        res = nb_adj(bord, stage+1, g)
        adjacent += len(res)
        val += res
    elif stage == hauteur-1:
        for k in range(g, d):
            bord += data[stage-1][k]
        res = nb_adj(bord, stage-1, g)
        adjacent += len(res)
        val += res
    else:
        for k in range(g, d):
            bord += data[stage+1][k]
        res = nb_adj(bord, stage+1, g)
        adjacent += len(res)
        val += res

        bord = ''
        for k in range(g, d):
            bord += data[stage-1][k]
        res = nb_adj(bord, stage-1, g)
        adjacent += len(res)
        val += res
    if data[stage][g].isdigit():
        res = num(stage, g)
        adjacent += 1
        val += [res]
    if data[stage][d-1].isdigit():
        res = num(stage, d-1)
        adjacent += 1
        val += [res]
    if len(val) == 2:
        return int(val[0])*int(val[1])
    else:
        return 0

somme = 0
etage = 0



for ligne in data:
    i=0
    ligne_temp = ligne
    i = select_star(ligne_temp, i)
    while i >=0:
        somme += search_gear(etage, i)
        i += 1
        ligne_temp=ligne[i:]
        i = select_star(ligne_temp, i)
    etage += 1

print(somme)
#53648804 trop petit

