class Map:
    def __init__(self, source, dest):
        self.source = source
        self.dest = dest
        self.map = [] #liste de tuples

    def table(self, val_dest, val_source, longeur):
        self.map += [(int(val_dest), int(val_source), int(longeur))]

    def conversion(self, val_source):
        for tbl in self.map:
            if tbl[1] <= val_source and val_source < tbl[1] + tbl[2]:
                return tbl[0] + (val_source - tbl[1])
        return val_source

    def passage(self, val_deb, val_long, i):
        a, b = val_deb, val_long
        res = []
        t = len(self.map)
        if i < t:
            d = self.map[i][1]
            k = self.map[i][2]
            if d+k<=a or a+b <= d:
                res += self.passage(a, b, i+1)
            elif (d <= a and d + k < a + b):
                res += [(self.conversion(a), d+k-a)]+self.passage(d+k, a+b-d-k, i+1)
            elif (a < d and  a + b <= d + k):
                res += self.passage(a, d-a, i+1) + [(self.conversion(d), a + b -d)]
            elif (a < d and d + k < a + b):
                res += self.passage(a, d-a, i+1) + [(self.conversion(d), k)] +  self.passage(d+k, a+b-d-k, i+1)
            elif (d <= a and a + b <= d + k):
                res += [(self.conversion(a), b)]
        if i >= t and b > 0:
            res += [(a, b)]
        return res


    def __str__(self):
        return 'source:' +  str(self.source) + '\ndestination: ' + str(self.dest) + '\ntables de conversion: ' + str(self.map)



data = open('day5.txt').read().splitlines()

#data = open('day5_test.txt').read().splitlines()

premiere_ligne = data[0].split()
graines = [(int(premiere_ligne[i]), int(premiere_ligne[i+1])) for i in range(1, len(premiere_ligne), 2)]


seed_to_soil = Map('seed', 'soil')
soil_to_fertilizer = Map('soil', 'fertilizer')
fertilizer_to_water = Map('fertilizer', 'water')
water_to_light = Map('water', 'light')
light_to_temperature = Map('light', 'temperature')
temperature_to_humidity = Map('temperature', 'humidity')
humidity_to_location = Map('humidity', 'location')


for ligne in data[2:]:
    if len(ligne)>0 and not ligne[0].isdigit():
        [source, to, dest] = ligne.split('-')
    match source:
       case 'seed':
           obj = seed_to_soil
       case 'soil':
           obj = soil_to_fertilizer
       case 'fertilizer':
           obj = fertilizer_to_water
       case 'water':
           obj = water_to_light
       case 'light':
           obj = light_to_temperature
       case 'temperature':
           obj = temperature_to_humidity
       case 'humidity':
           obj = humidity_to_location
    if len(ligne)>0 and ligne[0].isdigit():
        [vd, vs, t] = ligne.split()
        obj.table(vd, vs, t)



locations = []
soils = []
for lot_graine in graines:
    soils += seed_to_soil.passage(lot_graine[0], lot_graine[1], 0)

ferts=[]
for soil in soils:
    ferts += soil_to_fertilizer.passage(soil[0], soil[1], 0)

waters = []
for fert in ferts:
    waters += fertilizer_to_water.passage(fert[0], fert[1], 0)

lights = []
for water in waters:
    lights += water_to_light.passage(water[0], water[1], 0)

temps = []
for light in lights:
    temps += light_to_temperature.passage(light[0], light[1], 0)

hums = []
for temp in temps:
    hums += temperature_to_humidity.passage(temp[0], temp[1], 0)

for hum in hums:
    locations += humidity_to_location.passage(hum[0], hum[1], 0)

loc = [deb[0] for deb in locations]
print(min(loc))

