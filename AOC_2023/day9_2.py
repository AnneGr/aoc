#data = open('day9_test.txt').read().splitlines()

data = open('day9.txt').read().splitlines()

def diff(seq):
    n = len(seq)
    res = []
    for i in range(1, n):
        res += [seq[i]-seq[i-1]]
    return res
def isZero(seq):
    res = False
    for i in range(len(seq)):
        if seq[i] == 0:
            res = True
        else:
            return False
    return res

def back(seq):
    res = 0
    for i in range(len(seq)-2,-1, -1):
        res = seq[i][0] - res
    return res

predict_back = 0
for ligne in data:
    sequence = [int(i) for i in ligne.split()]
    suite = [sequence]
    sequence = diff(sequence)
    suite += [sequence]
    while not isZero(sequence):
        sequence = diff(sequence)
        suite += [sequence]
    predict_back += back(suite)
    

print(predict_back)
