data = open('day16_test.txt').read().splitlines()

temps = 0
pression = 0

voisin = dict()
visite = dict()
flow = dict()

def suivant_plus_grand(depart, dictionnaire, p_grand, dict_visite):
    liste = dictionnaire[depart]
    res = depart
    for elt in liste:
        if dict_visite[elt] == 0 and flow[elt] > p_grand:
            p_grand = flow[elt]
            res = elt
            dict_visite[elt] = 1
    return res, p_grand

def chemin_plus_grand(depart, dictionnaire, dict_visite, tps):
    print('tps',tps)
    plus_grand = 0
    res = depart
    liste = dictionnaire[depart]
    dict_visite[depart] = 0
    res, plus_grand = suivant_plus_grand(depart, dictionnaire, plus_grand, dict_visite)
    if plus_grand >= (30 - tps)//3:
        return [depart] + [res]
    elif tps == 30:
        return [depart] + []
    else:
        for i in range(30 - tps):
            print(3, depart, liste)
            tps += 1
            press = [suivant_plus_grand(elt, dictionnaire, dict_visite, tps)[1] for elt in liste]
            if max(press) >= (30 - tps)//4:
                return 
        
            
    
def parcours_optimal(dictionnaire, depart):
    temps = 1
    pression_parcourue = flow
    parcours = []
    while temps <= 30:
        parcours += suivant_plus_grand(depart, dictionnaire, visite, temps)
        print('p', parcours)
        temps += temps + len(parcours)
        depart = parcours.pop()
        for elt in parcours:
            pression_parcourue[elt] = 0
        print(temps)
        if temps > 4:
            break
    print('pression parcourue', pression_parcourue)
    print('parcours', parcours)
    return parcours
        
   

for ligne in data:
    res = ligne.split()
    suivants = res[9:]
    voisin[res[1]] = []
    visite[res[1]] = 0
    for elt in suivants:
        voisin[res[1]] += [elt[:2]]
    flow[res[1]] = int(res[4][5:-1])
print(voisin)
valve = 'AA'
#way = ['DD', 'CC', 'BB', 'AA','II', 'JJ', 'II', 'AA', 'DD', 'EE', 'FF', 'GG', 'HH', 'GG', 'FF', 'EE', 'DD', 'CC']


way = parcours_optimal(voisin, valve)
a_ouvrir = set(flow.values())

while temps < 30:
    distance = 1
    if way != []:
        valve = way.pop(0)
        temps += 1
        print('minute', temps, 'move', valve)
        #if flow[valve] > (30 - temps) //2 : 
        if flow[valve] != 0:
            temps += 1
            print('minute', temps, 'open', valve)
            pression += flow[valve] * (30 - temps)
            print('minute', temps, 30 - temps, flow[valve], pression)
            a_ouvrir.remove(flow[valve])
            flow[valve] = 0
        #elif flow[valve] != 0:
            #temps += 1
            #print('minute', temps, 'open', valve)
            #pression += flow[valve] * (30 - temps)
            #print('minute', temps, 30 - temps, flow[valve], pression)
            #flow[valve] = 0
            
    else:
        temps += 1
        print('minute', temps)
        print('pression', pression)
    
    
    
    
