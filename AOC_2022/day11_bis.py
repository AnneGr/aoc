data = open('day11.txt').read().splitlines()

k = 0 # rang du singe

singe = dict()
items = dict()
compteur = dict()
# singe[k][0] -> (operateur, operande)
# singe[k][1] -> diviseur pour le test
# singe[k][2] -> throwV
# singe[k][3] -> throwF

m = 19*13*5*7*17*2*3*11

def extraction(ligne):
    valeurs = []
    for i in range(len(ligne) - 1):
        if i>1:
            valeurs += [int(ligne[i][:-1])]
    return valeurs + [int(ligne[len(ligne)-1])]


def operation(operateur, operande, val):
    if operateur == '*':
        if operande == 'old':
            return val * val
        else:
            return val * int(operande)
    elif operateur == '+':
        if operande == 'old':
            return val +val
        else:
            return val + int(operande)
 
def test(diviseur, val):
    if val % diviseur == 0:
        return True
    else:
        return False

def effectuer_round(singe):
    for j in singe.keys(): 
        while items[j] != []:
            val = items[j].pop(0)
            current = operation(singe[j][0][0], singe[j][0][1], val) % m
            if test(singe[j][1], current):
                items[singe[j][2]]+= [current]
                compteur[j] += 1
            else:
                items[singe[j][3]]+= [current]
                compteur[j] += 1
            


for ligne in data:
    ligne = ligne.split()
    if ligne != []:
        if ligne[0] == 'Monkey':
            k = int(ligne[1][0])
            compteur[k] = 0
        elif ligne[0] == 'Starting':
            items[k] = extraction(ligne)
        elif ligne[0] == 'Operation:':
            operateur = ligne[4]
            operande = ligne[5]
            singe[k] = [(operateur, operande)]
        elif ligne[0] == 'Test:':
            diviseur = int(ligne[3])
            singe[k] += [diviseur]
        elif ligne[0] == 'If':
            if ligne[1] == 'true:': 
                throwV = int(ligne[5])
                singe[k] += [throwV]
            else:
                throwF = int(ligne[5])
                singe[k] += [throwF]
  


for p in range(10000):
   effectuer_round(singe)
   if p % 100 == 0:
       print(p)
print(singe)
print(' ')
#print(items)
print(compteur)

ordre = sorted(compteur.values())

m = ordre.pop()
n = ordre.pop()
print(ordre)
print(m, n, m*n)


