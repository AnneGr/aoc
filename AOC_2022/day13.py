data = open('day13.txt').read().splitlines()

def compare(p, q):
    """
    compare les entiers p et q ou les listes p et q
    fonction récursive
    
    renvoie 1 si les listes sont dans le 'bon ordre' (AOC jour 13), -1 sinon, 0 si ne sait pas
    """
    if type(p) ==  int and type(q) == int:
        print(p, q, 'entiers')
        if p < q:
            return 1
        elif p > q:
            return -1
        else:
            print('p=q', p, q)
            return 0
    elif type(p) == int:
        n = [p]
        print(n, p, q, 'p entier, q liste')
        return compare(n, q)
    elif type(q) == int:
        n = [q]
        print(p, q, n, 'q entier, p liste')
        return compare(p, n)
    elif len(p) == 0 and len(q)==0:
        return 0
    elif len(p) != 0 and len(q) != 0:
        lp = len(p)
        lq = len(q)
        c = 0
        print(p, q, 'listes')
        for u in range(min(lp, lq)):
            comp = compare(p[u], q[u])
            if comp == 1:
                return 1
            elif comp == -1:
                return -1
        if lp < lq:
            return 1
        elif lp > lq:
            return -1
        else:
            return 0
    elif len(p) == 0:
        return 1
    elif len(q) == 0:
        return -1
    else:
        return 0
        
        



paquet = []
for ligne in data:
    if ligne != '':
        ligne = eval(ligne)
        paquet += [ligne]
        
taille = len(paquet)

compteur = 0        

for i in range(taille//2):
    print('paire ', i+1)
    l = paquet[2*i]
    m = paquet[2*i+1]
    print(l)
    print(m)
    comp = compare(l,m)
    if comp != -1:
        compteur += (i+1)*comp
    print(comp)

print(compteur)

    
