data = open('day10.txt').read().splitlines()


x = 1
registre = [1]

for ope in data:
    ope = ope.split()
    if ope[0] == 'noop':
        registre = registre + [x]
    else:
        registre = registre + [x] + [x]
        x = x + int(ope[1])


print(registre)

for i in range(6):
    for k in range(1, 41):
        t = 40*i+k
        if (k == registre[t]+1 or k == registre[t] or k == registre[t]+2):
            print('#', end="")
        else:
            print('.', end="")
        
    print('')
    
    
