#import re
data = open('day5_test2.txt').read().splitlines()

p1 = ['M', 'J', 'C', 'B', 'F', 'R', 'L', 'H']
p2 = ['Z', 'C', 'D']
p3 = ['H', 'J', 'F', 'C', 'N', 'G', 'W']
p4 = ['P', 'J', 'D', 'M', 'T', 'S', 'B']
p5 = ['N', 'C', 'D', 'R', 'J']
p6 = ['W', 'L', 'D', 'Q', 'P', 'J', 'G', 'Z']
p7 = ['P', 'Z', 'T', 'F', 'R', 'H']
p8 = ['L', 'V', 'M', 'G']
p9 = ['C', 'B', 'G', 'P', 'F', 'Q', 'R', 'J']

piles = [[], p1, p2, p3, p4, p5, p6, p7, p8, p9]

#test = [[], ['Z', 'N'], ['M', 'C', 'D'], ['P']]

def deplace(nb, a, b):
    for i in range(nb):
        piles[b].append(piles[a].pop())
     
for ligne in data:
    ligne = ligne.split()
    k = int(ligne[1])
    debut = int(ligne[3])
    fin = int(ligne[5])
    deplace(k, debut, fin)
    
resultat = ''
for i in range(1, 10):
    resultat += piles[i].pop()
print(resultat)
    
    


    
    
