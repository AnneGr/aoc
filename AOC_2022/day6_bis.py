data = open('day6.txt').read().splitlines()

def recherche(chaine):
    for i in range(14, len(chaine)):
        motif = chaine[i-14:i]
        ens = set(motif)
        if len(ens) == 14:
            return i
#print(data)
print(recherche(data[0]))

#k = 0
#for ligne in data:
#    print('ligne',k, recherche(ligne))
#    k += 1
