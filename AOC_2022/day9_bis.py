data = open('day9.txt').read().splitlines()

# position de départ
position = []
for k in range(10):
    position.append([0, 0])


visited = set()

def tail(position):
    x = position[0][0]
    y = position[0][1]
    for i in range(1, 10):
        print(i)
        a = position[i][0] 
        b = position[i][1]
        print(x, y)

        print(a, b)
        if x == a:
            if y-b == 2 or b-y == 2:
                b += (y-b)//2
        elif y == b:
            if x-a == 2 or a-x == 2:
                a += (x-a)//2
        elif (x-a == 1 or a-x == 1) and (y-b == 2 or b-y == 2):
            a = x
            b += (y-b)//2
        elif (y-b == 1 or b-y == 1) and (x-a == 2 or a-x == 2):
            a += (x-a)//2
            b = y
        elif (y-b == 2 or b-y == 2) and (x-a == 2 or a-x == 2):
            a += (x-a)//2
            b += (y-b)//2
        position[i][0] = a
        position[i][1] = b
        print(a, b)
        x = a
        y = b
        print(x, y)
        
    visited.add((position[9][0], position[9][1]))
    return position

    
    
def mouvement(W, n, position):
    # W direction: R -> right, L -> left, U -> up, D -> down
    # n nombre de déplacements dans la direction W
    # position -> tableau des positions de la tête et des 9 autres noeuds
 

    for k in range(n):
        if W == 'U':
            position[0][1] += 1
        elif W == 'D':
            position[0][1]  -= 1
        elif W == 'R':
            position[0][0] += 1
        elif W == 'L':
            position[0][0] -= 1
        position = tail(position)
    return position
        
        


for deplacement in data:
    deplacement = deplacement.split()
    position = mouvement(deplacement[0], int(deplacement[1]), position)
    
    
print(visited)
print(len(visited))
