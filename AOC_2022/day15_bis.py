# coding: utf-8

data = open('day15.txt').read().splitlines()

donnees = []


beacon_sensor = set()

cible =1 #- 10 pour test
    
def union(liste, a, b):
    x, y = min(a, b), max(a, b) 
    new = []
    n = len(liste)//2
    if n == 0:
        return [x, y]
    i = 0
    j = 0
    fin = False
    while i < n and not fin:
        if x < liste[2*i]:
            debut = x
            j = i
            fin = True
        elif x<= liste[2*i + 1]:
            debut = liste[2*i]
            j = i
            fin = True
        else:
            new += [liste[2*i], liste[2*i + 1]]
            i += 1
            j = i
    if not fin:
        new += [x, y]
        return new
    while j < n:
        if y < liste[2*j]:
            new += [debut, y] + liste[2*j:]
            return new
        elif y <= liste[2*j + 1]:
            new += [debut, liste[2*j+1]]+liste[2*(j+1):]
            return new
        elif n - j == 1:
            new += [debut, y]
            return new
        elif y < liste[2*(j+1)]:
            new += [debut, y] + liste[2*(j+1):]
            return new
        else:
            j += 1
    new += [debut, y]
    return new


for ligne in data:
    ligne = ligne.split()
    donnees += [[(int(ligne[2][2:-1]), int(ligne[3][2:-1])), (int(ligne[8][2:-1]), int(ligne[9][2:]))]]

for ligne in donnees:
        xB, yB = ligne[1]
        beacon_sensor.add((xB, yB))
        xS, yS = ligne[0]
        beacon_sensor.add((xS, yS))

no_beacon = [0, 4000000]
cible = 2855040
while no_beacon == [0, 4000000] and cible < 4000001:
    cible += 1
    no_beacon = []
    for ligne in donnees:
        xB, yB = ligne[1]
        xS, yS = ligne[0]
        dist = abs(xS - xB) + abs(yS - yB)
        ecart = abs(cible - yS)
        reste = dist - ecart
        if reste > 0:
            no_beacon = union(no_beacon, max(xS-reste, 0), min(xS + reste, 4000000))
    print(no_beacon)
    print(cible)
print(2911363*4000000+cible)
