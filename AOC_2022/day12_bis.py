data = open('day12.txt').read().splitlines()

carte = [[c for c in ligne] for ligne in data]
visite = [[0 for c in ligne] for ligne in data]

colonnes = len(carte[0])
lignes = len(carte)

Depart = set()
for i in range(lignes):
    for j in range(colonnes):
        if carte[i][j] == 'a':
            Debut = (i, j)
            visite[i][j] = 1
            Depart.add((i, j))
        elif carte[i][j] == 'E':
            Arrivee = (i, j)
            carte[i][j] = 'z'

# sommets atteignables depuis le début en k étapes, k étant la clé  
chemin = dict()

def suivants(position):
    i, j = position
    resultat = set()
    if (i < lignes - 1) and (ord(carte[i+1][j]) - ord(carte[i][j]) <= 1) :
        resultat.add((i+1, j))
    if (i  > 0) and (ord(carte[i-1][j]) - ord(carte[i][j]) <= 1):
        resultat.add((i-1, j))
    if (j > 0) and (ord(carte[i][j-1]) - ord(carte[i][j]) <= 1):
        resultat.add((i, j-1))
    if (j < colonnes - 1) and (ord(carte[i][j+1]) - ord(carte[i][j]) <= 1):
        resultat.add((i, j+1))
    return resultat

hike = dict()

for point in Depart:
    visite = [[0 for c in ligne] for ligne in data]
    i, j = point
    k = 0
    chemin[k] = set()
    chemin[k].add(point)
    avancement = list(chemin[k])
    while (Arrivee not in avancement) and avancement != []:
        k += 1
        chemin[k] = set()
        while avancement != []:
            temp = avancement.pop(0)
            accessible = suivants(temp)
            ajouter = set()
            for sommet in accessible:
                i, j = sommet
                if visite[i][j] == 0:
                    ajouter.add((i, j))
                    visite[i][j] = carte[i][j]
            chemin[k].update(ajouter)
        avancement = list(chemin[k])
        if Arrivee in avancement:
            hike[point] = k
    


cle_depart = Debut
inf = 330
for depart in hike:
    if hike[depart] < inf:
        cle_depart = depart
        inf = hike[depart]

print(cle_depart, inf)
    
