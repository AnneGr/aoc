data = open('day11_test.txt').read().splitlines()

k = 0 # rang du singe

singe = [[], [], [], []] # 4 pour test - 8 pour données
#singe = [[], [], [], [], [], [], [], []]
# singe[k][0] -> liste des items
# singe[k][1] -> couple operateur, operande
# singe[k][2] -> diviseur pour le test
# singe[k][3] -> throwV
# singe[k][4] -> throwF
# singe[k][5] -> compteur de lancer

def extraction(ligne):
    valeurs = []
    for i in range(len(ligne) - 1):
        if i>1:
            valeurs += [int(ligne[i][:-1])]
    return valeurs + [int(ligne[len(ligne)-1])]

def operation(operateur, operande, val):
    if operateur == '*':
        if operande == 'old':
            return val * val
        else:
            return val * int(operande)
    elif operateur == '+':
        if operande == 'old':
            return val +val
        else:
            return val + int(operande)
    
def test(diviseur, val):
    if val % diviseur == 0:
        return True
    else:
        return False

def effectuer_round(singe):
    for j in range(4): # 8 pour les données 4 pour test
        
        while singe[j][0] != []:
            val = singe[j][0].pop(0)
            current = operation(singe[j][1][0], singe[j][1][1], val) // 3
            if test(singe[j][2], current):
                singe[singe[j][3]][0].append(current)
                singe[j][5] += 1
            else:
                singe[singe[j][4]][0].append(current)
                singe[j][5] += 1
            


for ligne in data:
    ligne = ligne.split()
   
    if ligne != []:
        
        if ligne[0] == 'Monkey':
            k = int(ligne[1][0])
        elif ligne[0] == 'Starting':
            singe[k] += [extraction(ligne)]
        elif ligne[0] == 'Operation:':
            operateur = ligne[4]
            operande = ligne[5]
            singe[k] += [(operateur, operande)]
        elif ligne[0] == 'Test:':
            diviseur = int(ligne[3])
            singe[k] += [diviseur]
        elif ligne[0] == 'If':
            if ligne[1] == 'true:': 
                throwV = int(ligne[5])
                singe[k] += [throwV]
            else:
                throwF = int(ligne[5])
                singe[k] += [throwF, 0]
  

for p in range(20):
   effectuer_round(singe)

compteur = []

for i in range(4):
    print(singe[i])
    compteur += [singe[i][5]]

ordre = sorted(compteur)
m = ordre.pop()
n = ordre.pop()
print(m, n, m*n)
    
    
