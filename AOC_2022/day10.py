data = open('day10.txt').read().splitlines()


x = 1
registre = [0]

for ope in data:
    ope = ope.split()
    if ope[0] == 'noop':
        registre = registre + [x]
    else:
        registre = registre + [x] + [x]
        x = x + int(ope[1])

if len(registre) <= 220:
    manque = 220 - len(registre)
    for k in range(manque):
        registre += [0]

resultat = registre[20] * 20 + registre[60] * 60 + registre[100] * 100 + registre[140] * 140 + registre[180] * 180 + registre[220] * 220

#print(registre)
print(resultat)
    
    
