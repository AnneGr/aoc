data = open('day6.txt').read().splitlines()

def recherche(chaine):
    for i in range(4, len(chaine)):
        motif = chaine[i-4:i]
        ens = set(motif)
        if len(ens) == 4:
            return i
#print(data)
print(recherche(data[0]))

#k = 0
#for ligne in data:
#    print('ligne',k, recherche(ligne))
#    k += 1
