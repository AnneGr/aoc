data = open('day3.txt').read().splitlines()

points = {}
resultat = 0
for i in range(97, 123):
    points[chr(i)] = i-96
for i in range(65, 91):
    points[chr(i)] = i-64+26
    
groupe = len(data)

for i in range(groupe//3):
    elfe1 = set(data[3*i])
    elfe2 = set(data[3*i+1])
    elfe3 = set(data[3*i+2])
    s = elfe1.intersection(elfe2)
    badge = s.intersection(elfe3)
    for x in badge:
        resultat += points[x]
    


print(resultat)
