data = open('day8.txt').read().splitlines()
l = len(data)
c = len(data[0])

visible=[]

# à refaire
def plus_grand(i, j):
    left = right = top = down = True
    
    for k in range(j):
        left = left and (data[i][j] > data[i][k])
    for k in range(j+1, c):
        right = right and (data[i][j] > data[i][k])
    for k in range(i):
        top = top and (data[i][j] > data[k][j])
    for k in range(i+1, l):
        down = down and (data[i][j] > data[k][j])
    return (left or right or top or down)

for i in range(l):
    temp = []
    visible.append([])
    for j in range(c):
        temp.append(data[i][j])
        visible[i].append(0)
    data[i] = temp

for j in range(c):
    visible[0][j] = 1
    visible[l-1][j] = 1
for i in range(l):
    visible[i][0] = 1
    visible[i][c-1] = 1

print(visible)
for i in range(1, l-1):
    for j in range(1, c-1):
        if plus_grand(i,j):
            visible[i][j] = 1

somme = 0
for i in range(l):
     for j in range(c):
         somme += visible[i][j]

print(visible)
print(somme)
