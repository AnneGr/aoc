data = open('day7.txt').read().splitlines()

nb_lignes = len(data)
repertoires_fils = dict()


taille_fichiers = dict()
taille_repertoires = dict()

place = '/'
k = 0

while (k < nb_lignes):
    ligne = data[k].split()
    if ligne[0] == '$':
        # seules possibilités: cd ou ls
        if ligne[1] == 'cd':
            if ligne[2] == '..':
                temp = place.split()
                if temp != []:
                    temp.pop()
                    place =  " ".join(temp)
                else:
                    place = '/'
            else:
                if ligne[2] != '/':
                    place = place + ' ' + ligne[2]
                else:
                    place = '/'
                if place not in repertoires_fils:
                    repertoires_fils[place] = []
            k += 1
        else:
            k += 1
            while (k < nb_lignes and data[k][0] != '$'):
                ligne = data[k].split()
                if ligne[0] == 'dir':
                    nouveau = place + ' ' + ligne[1]                    
                    repertoires_fils[place].append(nouveau)\
                    
                else:
                    nouveau = place+' ' + ligne[1]
                    repertoires_fils[place].append(nouveau)
                    taille_fichiers[nouveau] = int(ligne[0])
                k += 1
                
                
                
somme = 0

def taille_rep(rep):
    taille = 0
    for sous_rep in repertoires_fils[rep]:
        if sous_rep in taille_fichiers:
            taille = taille + taille_fichiers[sous_rep]
        else:
            taille += taille_rep(sous_rep)
    return taille

for rep in repertoires_fils:
    taille_repertoires[rep] = taille_rep(rep)

  
for rep in repertoires_fils:
    if  taille_repertoires[rep]<=100000:
        somme += taille_repertoires[rep]


print('somme',somme)
