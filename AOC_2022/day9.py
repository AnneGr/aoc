data = open('day9.txt').read().splitlines()

# position de départ
x, y = (0, 0)
a, b = (0 ,0)

visited = set()

def tail(pH, pT):
    x, y = pH
    a, b = pT
    if x == a:
        if y-b == 2 or b-y == 2:
            b += (y-b)//2
    elif y == b:
        if x-a == 2 or a-x == 2:
            a += (x-a)//2
    elif (x-a == 1 or a-x == 1) and (y-b == 2 or b-y == 2):
        a = x
        b += (y-b)//2
    elif (y-b == 1 or b-y == 1) and (x-a == 2 or a-x == 2):
        a += (x-a)//2
        b = y
    visited.add((a, b))
    return (a, b)

    
    
def mouvement(W, n, pH, pT):
    # W direction: R -> right, L -> left, U -> up, D -> down
    # n nombre de déplacements dans la direction W
    # pH -> position de Head (couple)
    # pT -> position de Tail (couple)
    x, y = pH
    a, b = pT
    for k in range(n):
        if W == 'U':
            y += 1
        elif W == 'D':
            y -= 1
        elif W == 'R':
            x += 1
        elif W == 'L':
            x -= 1
        a, b = tail((x, y), (a, b))
    return ((x, y), (a, b))
        
        
            
    

for deplacement in data:
    deplacement = deplacement.split()
    ((x, y), (a, b)) = mouvement(deplacement[0], int(deplacement[1]), (x, y), (a, b))
    
#print(visited)
print(len(visited))
