data = open('day15_test.txt').read().splitlines()

donnees = []


no_beacon = set()
beacon_sensor = set()

def finb(point, dist):
    x, y = point
    for i in range(dist + 1):
        for j in range(dist - i+1):
            if carte[y+j- y_min][x+i-x_min] == '.' :
                carte[y+j-y_min][x+i-x_min] = '#'
            if carte[y+j-y_min][x-i-x_min] == '.':
                carte[y+j-y_min][x-i-x_min] = '#'
            if carte[y-j-y_min][x+i-x_min] == '.':
                carte[y-j-y_min][x+i-x_min] = '#'
            if carte[y-j-y_min][x-i-x_min] == '.':
                carte[y-j-y_min][x-i-x_min] = '#'
            if y + j == 10:
                no_beacon.add(x +i)
                no_beacon.add(x - i)
            if y - j == 10:
                no_beacon.add(x + i)
                no_beacon.add(x - i)

for ligne in data:
    ligne = ligne.split()
    donnees += [[(int(ligne[2][2:-1]), int(ligne[3][2:-1])), (int(ligne[8][2:-1]), int(ligne[9][2:]))]]

x_min = x_max = donnees[0][0][0]
y_min = y_max = donnees[0][0][1]


for ligne in donnees:
    # recherche des coordonnées min et max des données
    Mx = max(ligne[0][0], ligne[1][0])
    My = max(ligne[0][1], ligne[1][1])
    mx = min(ligne[0][0], ligne[1][0])
    my = min(ligne[0][1], ligne[1][1])
    if  Mx > x_max:
        x_max = Mx
    if My > y_max:
        y_max = My
    if mx < x_min:
        x_min = mx
    if my < y_min:
        y_min = my

x_max = x_max + 20
x_min = x_min - 20
y_max = y_max + 20
y_min = y_min - 20
horizontal = x_max - x_min + 1
vertical = y_max - y_min + 1

carte = [['.' for _ in range(horizontal)] for _ in range(vertical)]

for ligne in donnees:
    xB, yB = ligne[1]
    carte[yB - y_min][xB - x_min] = 'B'
    beacon_sensor.add((xB, yB))
    xS, yS = ligne[0]
    x, y = xS - x_min, yS - y_min
    beacon_sensor.add((xS, yS))
    carte[y][x] = 'S'
    dist = abs(xS - xB) + abs(yS - yB)
    finb((xS, yS), dist)
  
for elt in beacon_sensor:
    x, y = elt
    if y == 10:
        no_beacon.discard(x)

#finb((8-x_min, 7-y_min),9 )

for ligne in carte:
    res = ''
    for c in ligne:
        res += c
    print(res)

print(no_beacon)
print(beacon_sensor)

print(len(no_beacon))
    
print(x_min, x_max, y_min, y_max)
        
        


