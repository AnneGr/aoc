data = open('day14.txt').read().splitlines()

def sable(u, v):
    if not dans_le_cadre(u+1, v):
        return u+1, v, 0
    else:
        if plan[u+1][v] == '.':
            return sable(u+1, v)
        else:
            if not dans_le_cadre(u+1, v-1):
                return u+1, v-1, 0
            else:
                if plan[u+1][v-1] == '.':
                    return sable(u+1, v-1)
                else:
                    if not dans_le_cadre(u+1, v+1):
                        return u+1, v+1, 0
                    else:
                        if plan[u+1][v+1] == '.':
                            return sable(u+1, v+1)
                        else:
                            if (u, v) == (0, 500-c_min):
                                plan[u][v] = 'o'
                                return u-1, v, 1
                            else:
                                plan[u][v] = 'o'
                            return u, v, 1
        
        

def dans_le_cadre(u, v):
    return (u >=0 and u <= l_max) and (v >= 0 and v <= c_max-c_min+1 )
rock = []

c_max = 0
c_min = 500
l_max = 0
l_min = 0

for ligne in data:
    ligne = ligne.replace('->', '')
    ligne = ligne.split()
    for i in range(len(ligne)):
        ligne[i] = eval(ligne[i])
        c, l = ligne[i]
        if c > c_max:
            c_max = c
        if c < c_min:
            c_min = c
        if l > l_max:
            l_max = l
    rock += [ligne]
    
#print(rock)

print(c_max, c_min, l_max, l_min)

largeur = c_max-c_min+1

l_max = l_max + 2
c_max += 4*largeur
c_min -= 4*largeur

largeur = c_max-c_min+1

plan = [['.']*largeur for j in range(l_max+1)]

# depart du sable (0, 500 - c_min)
plan[0][500-c_min] = '+'

# rocher
for ligne in rock:
    j, i = ligne[0][0], ligne[0][1]
    plan[i][j-c_min] = '#'
    l = len(ligne)
    if l > 1:
        for u in range(1, l):
            q, p = ligne[u][0], ligne[u][1]
            if q == j:
                s = abs(p-i)//(p-i)
                for a in range(i, p+s, s):
                    plan[a][j-c_min] = '#'
            elif p == i:
                s = abs(q-j)//(q-j)
                for a in range(j, q+s, s):
                    plan[i][a-c_min] = '#'
            i, j = p, q

plan[l_max] = ['#' for i in range(largeur)]

dedans = True

compteur = 0

while dedans:
    i, j, c = sable(0, 500-c_min)
    compteur += c
    dedans = dans_le_cadre(i, j)
    

for ligne in plan:
    res = ''
    for car in ligne:
        res += car
    print(res)
    
print(compteur)
