data = open('day2.txt').read().splitlines()

p=0
vainqueur = {'C':'A', 'A': 'B', 'B':'C'}
perdant = {'A':'C', 'B':'A', 'C':'B'}

def points(outil):
    if outil == 'A':
        return 1
    elif outil == 'B':
        return 2
    else:
        return 3

def score(jeu):
    opposant = jeu[0]
    moi = jeu[2]
    if moi ==  'X':
        moi = 'A'
    elif moi == 'Y':
        moi = 'B'
    else:
        moi = 'C'
    if (opposant == moi):
        return points(moi) + 3
    elif (vainqueur[opposant] == moi):
        return points(moi) + 6
    else:
        return points(moi)
        
def score_bis(jeu):
    opposant = jeu[0]
    moi = jeu[2]
    if moi ==  'X':
        return points(perdant[opposant])
    elif moi == 'Y':
        return points(opposant) + 3
    else:
        return points(vainqueur[opposant]) + 6


        
for partie in data:
    #p += score(partie)
    p += score_bis(partie)
    
print(p)


        
        
    
    
