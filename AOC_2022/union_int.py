# coding: utf-8

def union(liste, x, y):
    """
    liste triée de longueur paire contenant les extrémités des intervalles
    x et y extrémités du nouvel intervalle
    """
    new = []
    n = len(liste)//2
    print(n)
    i = 0
    j = 0
    fin = False
    while i < n and not fin:
        if x < liste[2*i]:
            debut = x
            j = i
            fin = True
        elif x<= liste[2*i + 1]:
            debut = liste[2*i]
            j = i
            fin = True
        else:
            new += [liste[2*i], liste[2*i + 1]]
            i += 1
            j = i
    if not fin:
        new += [x, y]
        return new
    
    
    while j < n:
        if y < liste[2*j]:
            new += [debut, y] + liste[2*j:]
            return new
        elif y <= liste[2*j + 1]:
            new += [debut, liste[2*j+1]]+liste[2*(j+1):]
            return new
        elif n == 1:
            new += [debut, y]
            return new
        elif y < liste[2*(j+1)]:
            new += [debut, y] + liste[2*(j+1):]
            return new
        else:
            j += 1
    
    new += [debut, y]
    
    return new

ensemble_depart = [6, 10]
test = [0, 10]
res = union(ensemble_depart, 0, 5)
print(res)

print(test == res)
    
