data = open('day3.txt').read().splitlines()

points = {}
resultat = 0
for i in range(97, 123):
    points[chr(i)] = i-96
for i in range(65, 91):
    points[chr(i)] = i-64+26
    
for ligne in data:
    taille = len(ligne)
    moitie = taille//2
    poche1 = set()
    poche2 = set()
    for i in range(moitie):
        poche1.add(ligne[i])
        poche2.add(ligne[taille-i-1])
    ensemble = poche1.intersection(poche2)
    for x in ensemble:
        resultat += points[x]
    


print(resultat)
