data = open('day7.txt').read().splitlines()

nb_lignes = len(data)
repertoires_fils = dict()
repertoires_pere = dict()

taille_fichiers = dict()
taille_repertoires = dict()

place = '/'
k = 0

while (k < nb_lignes):
    ligne = data[k].split()
    if ligne[0] == '$':
        # seules possibilités: cd ou ls
        if ligne[1] == 'cd':
            if ligne[2] == '..':
                if place != '/':
                    place = repertoires_pere[place]
            else:
                place = ligne[2]
                if place not in repertoires_fils:
                    repertoires_fils[place] = []
            k += 1
        else:
            k += 1
            while (k < nb_lignes and data[k][0] != '$'):
                ligne = data[k].split()
                if ligne[0] == 'dir':
                    if ligne[1] not in repertoires_fils:
                        nom = ligne[1]
                    else:
                        nom = ligne[1]+str(k)
                    repertoires_pere[nom] = place
                    repertoires_fils[place].append(nom)
                    
                else:
                    if ligne[1] not in repertoires_fils:
                        nom = ligne[1]
                    else:
                        nom = ligne[1]+str(k)
                    repertoires_pere[nom] = place
                    repertoires_fils[place].append(nom)
                    taille_fichiers[nom] = int(ligne[0])
                k += 1
                
                
somme = 0

def taille_rep(rep):
    taille = 0
    for sous_rep in repertoires_fils[rep]:
        if sous_rep in taille_fichiers:
            taille = taille + taille_fichiers[sous_rep]
        else:
            taille += taille_rep(sous_rep)
    return taille

for rep in repertoires_fils:
    taille_repertoires[rep] = taille_rep(rep)

  
for rep in repertoires_fils:
    if  taille_repertoires[rep]<=100000:
        somme += taille_repertoires[rep]
        print(taille_repertoires[rep])
        
print(taille_fichiers)
print(taille_repertoires)

print('somme',somme)
