data = open('day13.txt').read().splitlines()

def compare(p, q):
    """
    compare les entiers p et q ou les listes p et q
    fonction récursive
    
    renvoie 1 si les listes sont dans le 'bon ordre' (AOC jour 13), -1 sinon, 0 si ne sait pas
    """
    if type(p) ==  int and type(q) == int:
        if p < q:
            return 1
        elif p > q:
            return -1
        else:
            return 0
    elif type(p) == int:
        n = [p]
        return compare(n, q)
    elif type(q) == int:
        n = [q]
        return compare(p, n)
    elif len(p) == 0 and len(q)==0:
        return 0
    elif len(p) != 0 and len(q) != 0:
        lp = len(p)
        lq = len(q)
        c = 0
        for u in range(min(lp, lq)):
            comp = compare(p[u], q[u])
            if comp == 1:
                return 1
            elif comp == -1:
                return -1
        if lp < lq:
            return 1
        elif lp > lq:
            return -1
        else:
            return 0
    elif len(p) == 0:
        return 1
    elif len(q) == 0:
        return -1
    else:
        return 0
        
        



paquet = []
for ligne in data:
    if ligne != '':
        ligne = eval(ligne)
        paquet += [ligne]
 
paquet += [[[2]]]
paquet += [[[6]]]

taille = len(paquet)


def tri_fusion(tableau):
    n = len(tableau)
    if len(tableau)<=1:
        return tableau
    else:
        return fusion(tri_fusion(tableau[:n//2]), tri_fusion(tableau[n//2:]))

def fusion(tab1, tab2):
    if len(tab1) == 0:
        return tab2
    elif len(tab2) == 0:
        return tab1
    elif compare(tab1[0], tab2[0]) == 1:
        return [tab1[0]] + fusion(tab1[1:], tab2)
    else:
        return [tab2[0]] + fusion(tab1, tab2[1:])


paquet_trie = tri_fusion(paquet)

#for p in paquet_trie:
    #print(p)
    
compteur = 1
for i in range(taille+2):
    if paquet_trie[i] == [[2]]:
        compteur *= (i+1)
    elif paquet_trie[i] == [[6]]:
        compteur *= (i+1)
        break
print(compteur)




    
