data = open('day8.txt').read().splitlines()
l = len(data)
c = len(data[0])

visible=[]

def gauche(left, i, j):
    for k in range(j-1, -1, -1):
        if data[i][j] > data[i][k]:
            left = left + 1
        else:
            return left + 1
        
    return left

def droite(right, i, j):
    for k in range(j+1, c):
        if data[i][j] > data[i][k]:
            right = right + 1
        else:
            return right + 1
    return right

def haut(top, i, j):
    for k in range(i-1, -1, -1):
        if data[i][j] > data[k][j]:
            top = top + 1
        else:
            return top + 1
    return top


def bas(down, i, j):
    for k in range(i+1, l):
        if data[i][j] > data[k][j]:
            down = down + 1
        else:
            return down + 1
    return down

def visibilite(i, j):
    g = d = h = b = 0
    
    g = gauche(g, i, j)
        
    d = droite(d, i, j)
    
    h = haut(h, i, j)
    
    b = bas(b, i, j)
    
    return (g*d*h*b)

for i in range(l):
    temp = []
    visible.append([])
    for j in range(c):
        temp.append(data[i][j])
        visible[i].append(0)
    data[i] = temp


for i in range(1, l-1):
    for j in range(1, c-1):
        visible[i][j] = visibilite(i, j)


print(visible)

high = 0

for i in range(l):
    for j in range(c):
        if visible[i][j]>high:
            high = visible[i][j]
print(high)
