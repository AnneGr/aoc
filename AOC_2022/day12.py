data = open('day12.txt').read().splitlines()

carte = [[c for c in ligne] for ligne in data]
visite = [[0 for c in ligne] for ligne in data]

colonnes = len(carte[0])
lignes = len(carte)

for i in range(lignes):
    for j in range(colonnes):
        if carte[i][j] == 'S':
            Debut = (i, j)
            visite[i][j] = 1
            carte[i][j] = 'a'
        elif carte[i][j] == 'E':
            Arrivee = (i, j)
            carte[i][j] = 'z'

# sommets atteignables depuis le début en k étapes, k étant la clé  
chemin = dict()
# clé: sommet -> valeur: père du sommet
pere = dict()

def suivants(position):
    i, j = position
    resultat = set()
    if (i < lignes - 1) and (ord(carte[i+1][j]) - ord(carte[i][j]) <= 1) :
        resultat.add((i+1, j))
    if (i  > 0) and (ord(carte[i-1][j]) - ord(carte[i][j]) <= 1):
        resultat.add((i-1, j))
    if (j > 0) and (ord(carte[i][j-1]) - ord(carte[i][j]) <= 1):
        resultat.add((i, j-1))
    if (j < colonnes - 1) and (ord(carte[i][j+1]) - ord(carte[i][j]) <= 1):
        resultat.add((i, j+1))
    return resultat

k = 0
chemin[0] = set()
chemin[0].add(Debut)
avancement = list(chemin[k])
#print(chemin)
#print(avancement)

while (Arrivee not in avancement) and avancement != []:
    k += 1
    chemin[k] = set()
    #print(k)
    #print('chemin[k]',chemin[k])
    
    while avancement != []:
        temp = avancement.pop(0)
        accessible = suivants(temp)
        ajouter = set()
        for sommet in accessible:
            i, j = sommet
            if visite[i][j] == 0:
                ajouter.add((i, j))
                pere[sommet] = temp
                visite[i][j] = carte[i][j]
        chemin[k].update(ajouter)
    avancement = list(chemin[k])
    #print('avancement',avancement)
print(avancement)
print(k)
#print('pere',pere)
u = 0
trajet = Arrivee
while trajet != Debut and u<400:
    i, j = trajet
    visite[i][j] = '*'
    #print(pere[trajet])
    trajet = pere[trajet]
    u += 1
    
for ligne in visite:
    res = ''
    for c in ligne:
        res += str(c)
    print(res)

    
